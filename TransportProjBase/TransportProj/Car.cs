﻿using System;
using System.Diagnostics;

namespace TransportProj
{
    public abstract class Car : IdObject
    {
        public enum CarType { Undefined = 0, Coupe = 1, CrazyCar = 2 }
        public enum DirectionType { Undefined = 0, Right = 1, Left = 2, Up = 3, Down = 4 }
        public enum AxisType { X = 0, Y = 1 }

        public City City { get; private set; }
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }
        public DirectionType Direction { get; protected set; }
        public AxisType PrioMoveAxis { get; set; }
        public bool JustBumpedIntoSomething { get; set; }
        public Passenger Passenger { get; private set; }


        protected Car(City city, int xPos, int yPos, DirectionType direction, Passenger passenger)
        {
            Debug.Assert(city != null);
            Debug.Assert(xPos >= 0 && xPos <= city.XMax);
            Debug.Assert(yPos >= 0 && yPos <= city.YMax);

            City = city;
            XPos = xPos;
            YPos = yPos;
            Direction = direction;
            Passenger = passenger;
        }

        public void PickupPassenger(Passenger passenger)
        {
            Debug.Assert(passenger != null);
            Debug.Assert(Passenger == null);

            Passenger = passenger;
            passenger.GetInCar(this);
        }

        public void DropOffPassenger()
        {
            Debug.Assert(Passenger != null);

            Passenger.GetOutOfCar();
            Passenger = null;
        }

        public bool HasPassenger()
        {
            return Passenger != null;
        }

        public virtual bool MoveRight()
        {
            if (!City.MoveCar(this, XPos + 1, YPos))
                return false;

            XPos++;
            Direction = DirectionType.Right;

            WritePositionToConsole();
            return true;
        }

        public virtual bool MoveLeft()
        {
            if (!City.MoveCar(this, XPos - 1, YPos))
                return false;

            XPos--;
            Direction = DirectionType.Left;

            WritePositionToConsole();
            return true;
        }

        public virtual bool MoveUp()
        {
            if (!City.MoveCar(this, XPos, YPos + 1))
                return false;

            YPos++;
            Direction = DirectionType.Up;

            WritePositionToConsole();
            return true;
        }

        public virtual bool MoveDown()
        {
            if (!City.MoveCar(this, XPos, YPos - 1))
                return false;

            YPos--;
            Direction = DirectionType.Down;

            WritePositionToConsole();
            return true;
        }

        public bool MoveCarForward()
        {
            if (Direction == DirectionType.Right)
                return MoveRight();

            if (Direction == DirectionType.Left)
                return MoveLeft();

            if (Direction == DirectionType.Up)
                return MoveUp();

            if (Direction == DirectionType.Down)
                return MoveDown();

            Debug.Assert(false, "Unknown direction");
            return false;
        }

        public bool MoveCarBackward()
        {
            if (Direction == DirectionType.Right)
                return MoveLeft();

            if (Direction == DirectionType.Left)
                return MoveRight();

            if (Direction == DirectionType.Up)
                return MoveDown();

            if (Direction == DirectionType.Down)
                return MoveUp();

            Debug.Assert(false, "Unknown direction");
            return false;
        }

        public bool TurnCarRightOrLeft()
        {
            var random = new Random();

            if (Direction == DirectionType.Right || Direction == DirectionType.Left)
                if (random.Next(2) == 0)
                {
                    if (!MoveUp())
                        return MoveDown();
                }
                else
                {
                    if (!MoveDown())
                        return MoveUp();
                }
            else if (Direction == DirectionType.Up || Direction == DirectionType.Down)
                if (random.Next(2) == 0)
                {
                    if (!MoveRight())
                        return MoveLeft();
                }
                else
                {
                    if (!MoveLeft())
                        return MoveRight();
                }
            else
                Debug.Assert(false, "Unknown direction");

            return true;
        }

        public virtual bool MoveAboutCar()
        {
            if (MoveCarForward())
                return true;

            return TurnCarRightOrLeft();
        }

        public virtual void WritePositionToConsole()
        {
            Console.WriteLine("Car is at x: {0} y: {1}", XPos, YPos);
        }
    }
}