﻿
using System;
using System.Diagnostics;

namespace TransportProj
{
    public class City
    {
        public int XMax { get; private set; }
        public int YMax { get; private set; }

        private readonly CityPosition[][] _cityGrid;


        public City(int xMax, int yMax)
        {
            Debug.Assert(xMax >= 0);
            Debug.Assert(yMax >= 0);

            XMax = xMax;
            YMax = yMax;
            _cityGrid = new CityPosition[xMax + 1][];

            for (var x = 0; x <= XMax; x++)
            {
                _cityGrid[x] = new CityPosition[yMax + 1];

                for (var y = 0; y <= YMax; y++)
                    _cityGrid[x][y] = new CityPosition();
            }
        }

        public Car AddCar(Car.CarType carType, int xPos, int yPos, Car.DirectionType direction)
        {
            Debug.Assert(xPos >= 0 && xPos <= XMax);
            Debug.Assert(yPos >= 0 && yPos <= YMax);

            var pos = _cityGrid[xPos][yPos];
            Debug.Assert(!pos.HasCarOrBuilding());

            var car = CarFactory.BuildCar(carType, this, xPos, yPos, direction);
            pos.Car = car;

            return car;
        }

        public Building AddBuilding(int xPos, int yPos)
        {
            Debug.Assert(xPos >= 0 && xPos <= XMax);
            Debug.Assert(yPos >= 0 && yPos <= YMax);

            var pos = _cityGrid[xPos][yPos];
            Debug.Assert(!pos.HasCarOrBuilding());

            var building = new Building(this, xPos, yPos);
            pos.Building = building;

            return building;
        }

        public Passenger AddPassenger(int startXPos, int startYPos, int destXPos, int destYPos)
        {
            Debug.Assert(startXPos >= 0 && startXPos <= XMax);
            Debug.Assert(startYPos >= 0 && startYPos <= YMax);
            Debug.Assert(destXPos >= 0 && destXPos <= XMax);
            Debug.Assert(destYPos >= 0 && destYPos <= YMax);

            var startPos = _cityGrid[startXPos][startYPos];
            Debug.Assert(!startPos.HasPassenger());

            var passenger = new Passenger(startXPos, startYPos, destXPos, destYPos, this);
            startPos.Passenger = passenger;

            return passenger;
        }

        public bool HasCarOrBuilding(int x, int y)
        {
            return _cityGrid[x][y].HasCarOrBuilding();
        }

        public bool MoveCar(Car car, int xTo, int yTo)
        {
            Debug.Assert(car != null);

            if (xTo < 0 || xTo > XMax)
                return false;

            if (yTo < 0 || yTo > YMax)
                return false;

            var fromPos = _cityGrid[car.XPos][car.YPos];
            var toPos = _cityGrid[xTo][yTo];

            Debug.Assert(fromPos.Car == car);

            if (toPos.HasCarOrBuilding())
                return false;

            toPos.Car = fromPos.Car;
            fromPos.Car = null;

            if (!toPos.Car.HasPassenger())
                return true;

            toPos.Passenger = fromPos.Passenger;
            fromPos.Passenger = null;

            return true;
        }
    }
}