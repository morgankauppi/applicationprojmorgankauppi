﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class CarFactory
    {
        public static Car BuildCar(Car.CarType type,
                                   City city,
                                   int xPos,
                                   int yPos,
                                   Car.DirectionType direction)
        {
            if (type == Car.CarType.Coupe)
                return new Coupe(city, xPos, yPos, direction, null);

            if (type == Car.CarType.CrazyCar)
                return new CrazyCar(city, xPos, yPos, direction, null);

            Debug.Assert(false, "Unknown car type");
            return null;
        }
    }
}