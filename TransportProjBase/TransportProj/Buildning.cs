﻿using System;
using System.Diagnostics;

namespace TransportProj
{
    public class Building : IdObject
    {
        public City City { get; private set; }
        public int XPos { get; protected set; }
        public int YPos { get; protected set; }


        public Building(City city, int xPos, int yPos)
        {
            Debug.Assert(city != null);
            Debug.Assert(xPos >= 0 && xPos <= city.XMax);
            Debug.Assert(yPos >= 0 && yPos <= city.YMax);

            XPos = xPos;
            YPos = yPos;
        }
    }
}