﻿using System;
using System.Diagnostics;

namespace TransportProj
{
    public class CrazyCar : Car
    {
        public CrazyCar(City city, int xPos, int yPos, DirectionType direction, Passenger passenger)
            : base(city, xPos, yPos, direction, passenger)
        {
            Debug.Assert(city != null);
        }

        public override bool MoveRight()
        {
            var random = new Random();

            if (random.NextDouble() < 0.6)
                base.MoveRight();

            return base.MoveRight();
        }

        public override bool MoveLeft()
        {
            var random = new Random();

            if (random.NextDouble() < 0.6)
                base.MoveLeft();

            return base.MoveLeft();
        }

        public override bool MoveUp()
        {
            var random = new Random();

            if (random.NextDouble() < 0.6)
                base.MoveUp();

            return base.MoveUp();
        }

        public override bool MoveDown()
        {
            var random = new Random();

            if (random.NextDouble() < 0.6)
                base.MoveDown();

            return base.MoveDown();
        }

        public override bool MoveAboutCar()
        {
            var random = new Random();

            // Try forward with 60% probability
            if (random.NextDouble() < 0.6)
                if (MoveCarForward())
                    return true;

            // Try turning 45 deg
            if (TurnCarRightOrLeft())
                return true;

            // Try backward
            return MoveCarBackward();
        }

        public override void WritePositionToConsole()
        {
            Console.WriteLine("Crazy Car {0} is at x: {1} y: {2}", Id, XPos, YPos);
        }
    }
}