﻿
namespace TransportProj
{
    public class Visual
    {
        public enum DirectionType { Undefined = 0, Right = 1, Left = 2, Up = 3, Down = 4 }
        public enum VisualType { Undefined = 0, Coupe = 1, CrazyCar = 2, Passenger = 3, PassengerDest = 4, Building = 5 }

        public int Id { get; set; }
        public int XPos { get; set; }
        public int YPos { get; set; }
        public int Direction { get; set; }
        public int Type { get; set; }
    }
}