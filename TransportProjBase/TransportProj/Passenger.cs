﻿using System;
using System.Diagnostics;

namespace TransportProj
{
    public class Passenger : IdObject
    {
        public int StartingXPos { get; private set; }
        public int StartingYPos { get; private set; }
        public int DestinationXPos { get; private set; }
        public int DestinationYPos { get; private set; }
        public Car Car { get; private set; }
        public City City { get; private set; }


        public Passenger(int startXPos, int startYPos, int destXPos, int destYPos, City city)
        {
            Debug.Assert(startXPos >= 0 && startXPos <= city.XMax);
            Debug.Assert(startYPos >= 0 && startYPos <= city.YMax);
            Debug.Assert(destXPos >= 0 && destXPos <= city.XMax);
            Debug.Assert(destYPos >= 0 && destYPos <= city.YMax);
            Debug.Assert(city != null);

            StartingXPos = startXPos;
            StartingYPos = startYPos;
            DestinationXPos = destXPos;
            DestinationYPos = destYPos;
            City = city;
        }

        public void GetInCar(Car car)
        {
            Debug.Assert(car != null);
            Debug.Assert(Car == null);
            Debug.Assert(GetCurrentXPos() == car.XPos && GetCurrentYPos() == car.YPos);

            Car = car;
            Console.WriteLine("Passenger got in car");
        }

        public void GetOutOfCar()
        {
            Debug.Assert(Car != null);

            Car = null;
            Console.WriteLine("Passenger got out of car");
        }

        public int GetCurrentXPos()
        {
            return Car == null ? StartingXPos : Car.XPos;
        }

        public int GetCurrentYPos()
        {
            return Car == null ? StartingYPos : Car.YPos;
        }

        public bool IsAtDestination()
        {
            return GetCurrentXPos() == DestinationXPos && GetCurrentYPos() == DestinationYPos;
        }

        public bool IsInCar()
        {
            return Car != null;
        }
    }
}