﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Web.Script.Serialization;

namespace TransportProj
{
    class Program
    {
        const string ApiUrl = @"http://localhost:46482/api/visuals";

        static void Main()
        {
            const int carCount = 7;
            const int buildingCount = 15;

            const int cityLength = 15;
            const int cityWidth = 10;

            Debug.Assert(carCount > 0);
            Debug.Assert(buildingCount >= 0);
            Debug.Assert(cityLength > 0);
            Debug.Assert(cityWidth > 0);

            var rand = new Random();

            do
            {
                var city = new City(cityLength - 1, cityWidth - 1);
                var webMap = new WebMap(city, ApiUrl);

                // Clear web map
                webMap.Clear();

                // Create cars, buildings, and passengers

                var cars = new List<Car>();

                for (var i = 0; i < carCount; i++)
                    for (int j = 0; j < 100; j++)
                    {
                        var x = rand.Next(cityLength);
                        var y = rand.Next(cityWidth);

                        if (city.HasCarOrBuilding(x, y))
                            continue;

                        var carType = i == 0 && carCount > 1 ? Car.CarType.CrazyCar : Car.CarType.Coupe;
                        cars.Add(city.AddCar(carType, x, y, (Car.DirectionType)(rand.Next(4) + 1)));

                        break;
                    }

                var buildings = new List<Building>();

                for (var i = 0; i < buildingCount; i++)
                    for (var j = 0; j < 100; j++)
                    {
                        var x = rand.Next(cityLength);
                        var y = rand.Next(cityWidth);

                        if (city.HasCarOrBuilding(x, y))
                            continue;

                        buildings.Add(city.AddBuilding(x, y));
                        break;
                    }

                Passenger passenger = null;

                for (var i = 0; i < 100; i++)
                {
                    var x1 = rand.Next(cityLength);
                    var y1 = rand.Next(cityWidth);
                    var x2 = rand.Next(cityLength);
                    var y2 = rand.Next(cityWidth);

                    if (city.HasCarOrBuilding(x1, y1) || city.HasCarOrBuilding(x2, y2))
                        continue;

                    passenger = city.AddPassenger(x1, y1, x2, y2);
                    break;
                }

                if (passenger == null)
                    Debug.Assert(false, "No passenger");

                foreach (var car in cars)
                    car.WritePositionToConsole();

                Console.WriteLine("Passenger waiting at x: {0} y: {1}", passenger.StartingXPos, passenger.StartingYPos);
                Console.WriteLine("Passenger destination is x: {0} y: {1}", passenger.DestinationXPos, passenger.DestinationYPos);

                // Add visuals to web map

                foreach (var car in cars)
                    webMap.AddVisual(new Visual
                     {
                         Id = car.Id,
                         XPos = car.XPos,
                         YPos = car.YPos,
                         Direction = (int)car.Direction,
                         Type = (int)GetVisualType(car)
                     });

                foreach (var building in buildings)
                    webMap.AddVisual(new Visual
                    {
                        Id = building.Id,
                        XPos = building.XPos,
                        YPos = building.YPos,
                        Type = (int)Visual.VisualType.Building
                    });

                webMap.AddVisual(new Visual
                {
                    Id = passenger.Id,
                    XPos = passenger.GetCurrentXPos(),
                    YPos = passenger.GetCurrentYPos(),
                    Type = (int)Visual.VisualType.Passenger
                });

                webMap.AddVisual(new Visual
                {
                    Id = -1,
                    XPos = passenger.DestinationXPos,
                    YPos = passenger.DestinationYPos,
                    Type = (int)Visual.VisualType.PassengerDest
                });

                // Animate until passenger is at destination

                var stopwatch = new Stopwatch();

                while (!passenger.IsAtDestination())
                {
                    stopwatch.Reset();
                    stopwatch.Start();

                    foreach (var car in cars)
                        Tick(car, passenger, webMap);

                    while (stopwatch.ElapsedMilliseconds < 200)
                        Thread.Sleep(0);
                }

                // Drop off passenger if any
                // NB: The car will not have a passenger if the passenger
                //     was already at the destination from the beginning
                foreach (var car in cars)
                    if (car.HasPassenger())
                        car.DropOffPassenger();

                Debug.Assert(!passenger.IsInCar());

                foreach (var car in cars)
                    Debug.Assert(!car.HasPassenger());
            } while (true);
        }

        /// <summary>
        /// Takes one action (move the car one spot or pick up the passenger).
        /// </summary>
        /// <param name="car">The car to move</param>
        /// <param name="passenger">The passenger to pick up</param>
        /// <param name="webMap">The web map to draw on</param>
        private static void Tick(Car car, Passenger passenger, WebMap webMap)
        {
            Debug.Assert(car != null);
            Debug.Assert(passenger != null);

            if (passenger.IsInCar() && passenger.Car != car)  // Move about car if passenger was picked up by another car
                car.JustBumpedIntoSomething = !car.MoveAboutCar();
            else if (car.JustBumpedIntoSomething)  // Try to get unstuck if bumped into something last tick
            {
                car.JustBumpedIntoSomething = !car.TurnCarRightOrLeft();

                if (car.JustBumpedIntoSomething)
                    car.JustBumpedIntoSomething = !car.MoveAboutCar();
            }
            else
            {
                // Find destination

                int carDestinationX, carDestinationY;

                if (!car.HasPassenger())
                {
                    carDestinationX = passenger.StartingXPos;
                    carDestinationY = passenger.StartingYPos;
                }
                else
                {
                    carDestinationX = passenger.DestinationXPos;
                    carDestinationY = passenger.DestinationYPos;
                }

                // Move in either x or y direction; examine the prioritized axis first

                for (var i = 0; i < 2; i++)
                    if (i == (int)car.PrioMoveAxis)
                    {
                        if (car.XPos < carDestinationX)
                            car.JustBumpedIntoSomething = !car.MoveRight();
                        else if (car.XPos > carDestinationX)
                            car.JustBumpedIntoSomething = !car.MoveLeft();
                        else
                            continue;  // Didn't need to move along this axis; check other axis

                        break;  // Don't move more than once
                    }
                    else
                    {
                        if (car.YPos < carDestinationY)
                            car.JustBumpedIntoSomething = !car.MoveUp();
                        else if (car.YPos > carDestinationY)
                            car.JustBumpedIntoSomething = !car.MoveDown();
                        else
                            continue;  // Didn't need to move along this axis; check other axis

                        break;  // Don't move more than once
                    }

                // Pick up passenger if possible
                if (!car.HasPassenger() &&
                    passenger.GetCurrentXPos() == car.XPos &&
                    passenger.GetCurrentYPos() == car.YPos)
                    car.PickupPassenger(passenger);

                // Try to get unstuck if bumped into something, and pick prioritized axis randomly to get around obstacle
                if (car.JustBumpedIntoSomething)
                {
                    car.JustBumpedIntoSomething = !car.MoveAboutCar();

                    var random = new Random();
                    car.PrioMoveAxis = (Car.AxisType)random.Next(2);
                }
            }

            // Update passenger graphics on web map
            if (car.HasPassenger())
                webMap.UpdateVisual(new Visual
                {
                    Id = passenger.Id,
                    XPos = passenger.GetCurrentXPos(),
                    YPos = passenger.GetCurrentYPos(),
                    Type = (int)Visual.VisualType.Passenger
                });

            // Update car graphics on web map
            webMap.UpdateVisual(new Visual
            {
                Id = car.Id,
                XPos = car.XPos,
                YPos = car.YPos,
                Direction = (int)car.Direction,
                Type = (int)GetVisualType(car)
            });
        }

        private static Visual.VisualType GetVisualType(Car car)
        {
            if (car.GetType().Name == "Coupe")
                return Visual.VisualType.Coupe;

            if (car.GetType().Name == "CrazyCar")
                return Visual.VisualType.CrazyCar;

            Debug.Assert(false, "Unknown type");
            return Visual.VisualType.Undefined;
        }
    }
}