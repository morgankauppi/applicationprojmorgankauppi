﻿using System;
using System.Diagnostics;

namespace TransportProj
{
    public class Coupe : Car
    {
        public Coupe(City city, int xPos, int yPos, DirectionType direction, Passenger passenger)
            : base(city, xPos, yPos, direction, passenger)
        {
            Debug.Assert(city != null);
        }

        public override bool MoveAboutCar()
        {
            var random = new Random();

            // Try forward with 90% probability
            if (random.NextDouble() < 0.9)
                if (MoveCarForward())
                    return true;

            // Try turning 45 deg
            if (TurnCarRightOrLeft())
                return true;

            // Try backward
            return MoveCarBackward();
        }

        public override void WritePositionToConsole()
        {
            Console.WriteLine("Coupe {0} is at x: {1} y: {2}", Id, XPos, YPos);
        }
    }
}