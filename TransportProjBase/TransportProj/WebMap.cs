﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;

namespace TransportProj
{
    class WebMap
    {
        public int XMax { get; private set; }
        public int YMax { get; private set; }
        public string ApiUrl { get; private set; }


        public WebMap(City city, string apiUrl)
        {
            Debug.Assert(city != null);

            XMax = city.XMax;
            YMax = city.YMax;
            ApiUrl = apiUrl;
        }

        public void AddVisual(Visual visual)
        {
            try
            {
                var client = new HttpClient();
                var response = client.PostAsync(ApiUrl, new StringContent(
                   new JavaScriptSerializer().Serialize(visual), Encoding.UTF8, "application/json")).Result;
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't connect to web service: Is it running? Is the url correct?");
                Console.ResetColor();
            }
        }

        public void UpdateVisual(Visual visual)
        {
            try
            {
                var client = new HttpClient();
                var response = client.PutAsync(ApiUrl + "/" + visual.Id, new StringContent(
                   new JavaScriptSerializer().Serialize(visual), Encoding.UTF8, "application/json")).Result;
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't connect to web service: Is it running? Is the url correct?");
                Console.ResetColor();
            }
        }

        public void DeleteVisual(int id)
        {
            try
            {
                var client = new HttpClient();
                var response = client.DeleteAsync(ApiUrl + "/" + id);
            }
            catch
            {
                Console.ForegroundColor = ConsoleColor.Red;
                Console.WriteLine("Can't connect to web service: Is it running? Is the url correct?");
                Console.ResetColor();
            }
        }

        public void Clear()
        {
            DeleteVisual(0);
        }
    }
}