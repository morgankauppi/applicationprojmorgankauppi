﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TransportProj
{
    public class IdObject
    {
        private static int _currentId;
        public int Id { get; private set; }

        public IdObject()
        {
            Id = ++_currentId;
        }
    }
}