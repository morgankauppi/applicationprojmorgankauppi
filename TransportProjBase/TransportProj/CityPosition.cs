﻿
namespace TransportProj
{
    public class CityPosition
    {
        public Car Car { get; set; }
        public Passenger Passenger { get; set; }
        public Building Building { get; set; }

        public bool HasCar() { return Car != null; }
        public bool HasCarOrBuilding() { return Car != null || Building != null; }
        public bool HasPassenger() { return Passenger != null; }
    }
}