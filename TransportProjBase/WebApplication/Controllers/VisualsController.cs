﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Web.Http;
using Microsoft.Ajax.Utilities;
using WebApplication.Models;

namespace WebApplication.Controllers
{
    public class VisualsController : ApiController
    {
        // GET api/visuals
        public IEnumerable<Visual> Get()
        {
            Monitor.Enter(SimpleDatabase.Visuals);

            try
            {
                var visualsCopy = new List<Visual>();
                SimpleDatabase.Visuals.CopyItemsTo(visualsCopy);

                return visualsCopy;
            }
            finally
            {
                Monitor.Exit(SimpleDatabase.Visuals);
            }
        }

        // GET api/visuals/<id>
        public Visual Get(int id)
        {
            Monitor.Enter(SimpleDatabase.Visuals);

            try
            {
                return SimpleDatabase.Visuals.SingleOrDefault(v => v.Id == id);
            }
            finally
            {
                Monitor.Exit(SimpleDatabase.Visuals);
            }
        }

        // POST api/visuals
        public void Post([FromBody]Visual visual)
        {
            Monitor.Enter(SimpleDatabase.Visuals);

            try
            {
                SimpleDatabase.Visuals.Add(visual);
            }
            finally
            {
                Monitor.Exit(SimpleDatabase.Visuals);
            }
        }

        // PUT api/visuals/<id>
        public void Put(int id, [FromBody]Visual visual)
        {
            Monitor.Enter(SimpleDatabase.Visuals);

            try
            {
                var existingVisual = SimpleDatabase.Visuals.SingleOrDefault(v => v.Id == id);

                if (existingVisual == null)
                    return;

                existingVisual.XPos = visual.XPos;
                existingVisual.YPos = visual.YPos;
                existingVisual.Direction = visual.Direction;
                existingVisual.Type = visual.Type;
            }
            finally
            {
                Monitor.Exit(SimpleDatabase.Visuals);
            }
        }

        // DELETE api/visuals/<id>
        public void Delete(int id)
        {
            Monitor.Enter(SimpleDatabase.Visuals);

            try
            {
                if (id == 0)
                    SimpleDatabase.Visuals.Clear();
                else
                {
                    var existingVisual = SimpleDatabase.Visuals.SingleOrDefault(v => v.Id == id);
                    SimpleDatabase.Visuals.Remove(existingVisual);
                }
            }
            finally
            {
                Monitor.Exit(SimpleDatabase.Visuals);
            }
        }
    }
}